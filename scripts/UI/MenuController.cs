using System;
using Godot;
using ZoomBees.scripts.Managers;

namespace ZoomBees.scripts.UI; 

public partial class MenuController : Control {
  public event Action ExitedMenu;
  
  protected GameManager GameMgr;

  public override void _Ready() {
    GameMgr = GetNode<GameManager>("/root/Game");
  }
  
  protected void OnExit() {
    ExitedMenu?.Invoke();
  }
}