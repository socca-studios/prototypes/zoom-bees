using Godot;
using ZoomBees.scripts.Managers;

namespace ZoomBees.scripts.UI; 

public partial class PauseMenu : MenuController {
  private Button _resumeBtn;
  private Button _saveBtn;
  private Button _loadBtn;
  private Button _exitBtn;
  
  public override void _Ready() {
	  base._Ready();
	  
		// Game Nodes
		base.GameMgr.ChangedPauseState += OnChangedGamePauseState;
		
		// Button Nodes
		_exitBtn = (Button)FindChild("ExitBtn");
		_exitBtn.Pressed += base.OnExit;
		_loadBtn = (Button)FindChild("LoadBtn");
		_loadBtn.Pressed += OnLoad;
		_resumeBtn = (Button)FindChild("ResumeBtn");
		_resumeBtn.Pressed += OnResume;
		_saveBtn = (Button)FindChild("SaveBtn");
		_saveBtn.Pressed += OnSave;

		ExitedMenu += GameMgr.ToMainMenu;
  }
  
  // Event-handlers
  private void OnChangedGamePauseState(bool pauseState) {
	  GD.Print($"{pauseState}");
	  if (pauseState) {
		  Show();
		  Input.MouseMode = Input.MouseModeEnum.Visible;
	  } else {
		  Hide();
		  Input.MouseMode = Input.MouseModeEnum.Captured;
	  }
  }
  
  private void OnLoad() {
	  // Pass
  }
  
  private void OnResume() {
	  base.GameMgr.IsGamePaused = false;
  }
  
  private void OnSave() {
	  // Pass
  }
}
