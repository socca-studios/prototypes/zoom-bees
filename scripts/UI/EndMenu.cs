using Godot;

namespace ZoomBees.scripts.UI; 

public partial class EndMenu : MenuController {
  private Button _playBtn;
  private Button _mainMenuBtn;
  private Button _quitGameBtn;

  public override void _Ready() {
    base._Ready();

    ((Label)FindChild("Kills")).Text = $"{GameMgr.Kills}";
    ((Label)FindChild("Headshots")).Text = $"{GameMgr.Headshots}";;
    ((Label)FindChild("Precision")).Text = $"{100f*GameMgr.Touches/GameMgr.Shots:n2} %";;
    
    _playBtn = (Button)FindChild("PlayBtn");
    _mainMenuBtn = (Button)FindChild("MainMenuBtn");
    _quitGameBtn = (Button)FindChild("QuitBtn");
    _playBtn.Pressed += OnPlay;
    _mainMenuBtn.Pressed += OnMainMenu;
    _quitGameBtn.Pressed += base.OnExit;
    
    base.ExitedMenu += GameMgr.ExitGame;
  }

  private void OnPlay() {
    GameMgr.LaunchLevel();
  }

  private void OnMainMenu() {
    GameMgr.ToMainMenu();
  }
}