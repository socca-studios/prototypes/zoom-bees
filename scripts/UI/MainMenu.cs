using Godot;

namespace ZoomBees.scripts.UI; 

public partial class MainMenu : MenuController {
  private Button _playBtn;
  private Button _optionBtn;
  private Button _quitGameBtn;

  public override void _Ready() {
    base._Ready();
    
    _playBtn = (Button)FindChild("PlayBtn");
    _optionBtn = (Button)FindChild("OptionBtn");
    _quitGameBtn = (Button)FindChild("QuitBtn");
    _playBtn.Pressed += OnPlay;
    _optionBtn.Pressed += OnOptions;
    _quitGameBtn.Pressed += base.OnExit;

    base.ExitedMenu += GameMgr.ExitGame;
  }

  private void OnPlay() {
    GameMgr.LaunchLevel();
  }

  private void OnOptions() {
    // Pass
  }
}