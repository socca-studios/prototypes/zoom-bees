using System;
using System.Collections.Generic;
using Godot;

namespace ZoomBees.scripts; 

public partial class GhoulController : CharacterBody3D {
  public event Action<Vector3> HitPlayer;
  public event Action Impacted;
  public event Action ImpactedCriticalPart;
  public event Action KilledGhoul;
  
  // Game Nodes
  private AnimationTree _animationTree;
  private GpuParticles3D _bloodParticles;
  private NavigationAgent3D _navAgent;
  private LevelController _level;
  
  private List<BodyPartController> _bodyParts;
  
  private AnimationNodeStateMachinePlayback _stateMachine;
  
  // Attributes
  private float _health = 5f;
  private float _speed = 4f;
  private float _attackRange = 2.5f;
  
  public override void _Ready() {
    CallDeferred("LateInit");
    _animationTree = (AnimationTree)FindChild("AnimationTree");
    _stateMachine = (AnimationNodeStateMachinePlayback)_animationTree.Get("parameters/playback");

    _bloodParticles = GetNode<GpuParticles3D>("BloodDrops");
    _bodyParts = new List<BodyPartController> {
      GetNode<BodyPartController>("Model/Skeleton/Head/Area3D"),
      GetNode<BodyPartController>("Model/Skeleton/Spine/Area3D"),
      GetNode<BodyPartController>("Model/Skeleton/ShoulderL/Area3D"),
      GetNode<BodyPartController>("Model/Skeleton/ShoulderL/Area3D"),
      GetNode<BodyPartController>("Model/Skeleton/LegL/Area3D"),
      GetNode<BodyPartController>("Model/Skeleton/LegR/Area3D")
    };

    foreach (BodyPartController bodyPart in _bodyParts) {
      bodyPart.ImpactedBodyPart += OnImpactedBodyPart;
    }
    
    _level = GetNode<LevelController>("/root/Game/Level");
  }

  private void LateInit() {
    _navAgent = (NavigationAgent3D)FindChild("NavigationAgent");
  }
  
  public override void _PhysicsProcess(double delta) {
    if (_navAgent is null) return;  // NavAgent is not loaded at the first physic frame
    
    Velocity = Vector3.Zero;

    switch (_stateMachine.GetCurrentNode()) {
      case "Attack":
        HandleAttack();
        break;
      case "Die":
        break;
      case "Move":
        HandleMove(delta);
        break;
      case "Start":
        break;
      case "Spawn":
        break;
      default:
        // GD.PushWarning($"{_stateMachine.GetCurrentNode()} is not a known state!");
        break;
    }
    
    _animationTree.Set("parameters/conditions/run", !TargetInRange());
    _animationTree.Set("parameters/conditions/attack", TargetInRange());
  }

  private void HandleMove(double delta) {
    _navAgent.TargetPosition = _level.Player.GlobalPosition;
    Vector3 targetPosition = _navAgent.GetNextPathPosition();
    Velocity = (targetPosition - GlobalPosition).Normalized() * _speed;
    Vector3 rotation = Rotation;
    rotation.Y = Mathf.Lerp(rotation.Y, Mathf.Atan2(-Velocity.X, -Velocity.Z), (float)delta * 10f);
    LookAt(new Vector3(GlobalPosition.X + Velocity.X, 0f, GlobalPosition.Z + Velocity.Z));
    MoveAndSlide();
  }

  private void HandleAttack() {
    LookAt(new Vector3(_level.Player.GlobalPosition.X, 0f, _level.Player.GlobalPosition.Z));
  }
  
  private bool TargetInRange(float rangeModifier = 0f) {
    return GlobalPosition.DistanceTo(_level.Player.GlobalPosition) <= _attackRange + rangeModifier;
  }

  private void HitFinished() {
    if (TargetInRange()) {
      Vector3 dir = GlobalPosition.DirectionTo(_level.Player.GlobalPosition);
      HitPlayer?.Invoke(dir);
    }
  }

  private void OnImpactedBodyPart(float damage, Vector3 impactedPosition, bool isCriticalPart) {
    if (isCriticalPart) ImpactedCriticalPart?.Invoke();
    Impacted?.Invoke();
    TakeDamage(damage);
    GD.Print($"Enemy: ouch! [health: {_health-damage}]");

    _bloodParticles.GlobalPosition = impactedPosition;
    _bloodParticles.Emitting = true;
  }
  
  private void TakeDamage(float damage) {
    _health -= damage;
    if (_health <= 0) {
      Die();
    }
  }

  private void Die() {
    _animationTree.Set("parameters/conditions/die", true);
    GetTree().CreateTimer(2f).Timeout += QueueFree;
    KilledGhoul?.Invoke();
  }
}