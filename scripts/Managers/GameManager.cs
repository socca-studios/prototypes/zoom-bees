using System;
using Godot;

namespace ZoomBees.scripts.Managers;

public partial class GameManager : Node {
  public event Action<bool> ChangedPauseState;

  public AssetManager Assets;
  public int Headshots;
  public int Kills;
  public int Shots;
  public int Touches;
  
  public bool IsGamePaused {
    get => _isGamePaused;
    set {
      _isGamePaused = value;
      GetTree().Paused = _isGamePaused;
      ChangedPauseState?.Invoke(_isGamePaused);
    }
  }

  private bool _isGamePaused;
  private Node _node;

  public override void _Ready() {
    Assets = GetNode<AssetManager>("/root/AssetManager");
    ToMainMenu();
  }
  
  public override void _Process(double delta) {
    if (Input.IsActionJustPressed("menu")) {
      IsGamePaused = !IsGamePaused;
    }
  }

  // Game State Change
  public void ExitGame() {
    GetTree().Quit();
  }

  public void LaunchLevel() {
    SwitchingToNode(Assets.Levels["level_0"]);
    Input.MouseMode = Input.MouseModeEnum.Captured;
    
    Headshots = 0;
    Kills = 0;
    Shots = 0;
    Touches = 0;
  }

  public void ToEndMenu() {
    SwitchingToNode(Assets.Menus["end_menu"]);
    IsGamePaused = false;
    Input.MouseMode = Input.MouseModeEnum.Visible;
  }
  
  public void ToMainMenu() {
    SwitchingToNode(Assets.Menus["main_menu"]);
    IsGamePaused = false;
    Input.MouseMode = Input.MouseModeEnum.Visible;
  }
  
  // Private Methods
  private void SwitchingToNode(PackedScene nodeScene) {
    _node?.QueueFree();
    _node = nodeScene.Instantiate<Node>();
    AddChild(_node);
  }
  
}