using Godot;
using Godot.Collections;

namespace ZoomBees.scripts.Managers; 

public partial class AssetManager : Node {
  // Constant Attributes
  private Dictionary<string, string> _actorPaths = new() {
    { "ghoul", "res://scenes/Monsters/Ghoul.tscn" },
    { "player", "res://scenes/Player.tscn" },
  };
  
  private Dictionary<string, string> _materialPaths = new() {
    // {"blue", "res://assets/materials/blue.tres"},
    // {"glow_red", "res://assets/materials/glowing_red.tres"},
    // {"glow_green", "res://assets/materials/glowing_green.tres"},
    // {"green", "res://assets/materials/green.tres"},
    // {"grey", "res://assets/materials/grey.tres"},
    // {"selection_yellow", "res://assets/materials/outline_yellow.tres"},
  };

  private Dictionary<string, string> _menuPaths = new() {
    { "end_menu", "res://scenes/UI/EndMenu.tscn" },
    { "main_menu", "res://scenes/UI/MainMenu.tscn" },
  };
  
  private Dictionary<string, string> _levelPaths = new() {
    { "level_0", "res://scenes/Levels/Level0.tscn" },
  };
  
  private Dictionary<string, string> _objectPaths = new() {
    { "bullet", "res://scenes/Weapons/Ammunitions/Bullet.tscn" },
    { "laser_beam", "res://scenes/Weapons/Ammunitions/LaserBeam.tscn" },
  };

  // Properties
  public Dictionary<string, PackedScene> Actors { get; private set; }
  public Dictionary<string, PackedScene> Levels { get; private set; }
  public Dictionary<string, Material> Mats { get; private set; }
  public Dictionary<string, PackedScene> Menus { get; private set; }
  public Dictionary<string, PackedScene> Objects { get; private set; }
  
  // Nodes
  public GameManager GameManager { get; private set; }
  
  // Initialising & Update
  // ----------------------------------------
  public override void _Ready() {
    GameManager = GetNode<GameManager>("/root/Game");
    LoadAssets();
  }
  
  // Methods
  // ----------------------------------------
  private void LoadAssets() {
    Actors = LoadScenes(_actorPaths);
    Levels = LoadScenes(_levelPaths);
    Mats = LoadMaterials(_materialPaths);
    Menus = LoadScenes(_menuPaths);
    Objects = LoadScenes(_objectPaths);
  }

  private static Dictionary<string, PackedScene> LoadScenes(Dictionary<string, string> assetPaths) {
    Dictionary<string, PackedScene> scenes = new();

    foreach (string asset in assetPaths.Keys) {
      scenes.Add(asset, GD.Load<PackedScene>(assetPaths[asset]));  
    }

    return scenes;
  }
  
  private static Dictionary<string, Material> LoadMaterials(Dictionary<string, string> assetPaths) {
    Dictionary<string, Material> materials = new();

    foreach (string asset in assetPaths.Keys) {
      materials.Add(asset, GD.Load<Material>(assetPaths[asset]));  
    }

    return materials;
  }
}