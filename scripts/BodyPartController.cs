using System;
using Godot;

namespace ZoomBees.scripts; 

public partial class BodyPartController : Area3D, IDamageable {
  public event Action<float, Vector3, bool> ImpactedBodyPart; 
  
  [Export] public float Damage = 1f;
  [Export] public bool IsCriticalPart = false;

  public void Hit(Vector3 impactPosition) {
    ImpactedBodyPart?.Invoke(Damage, impactPosition, IsCriticalPart);
  }
}