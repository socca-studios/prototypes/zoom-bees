using System;
using System.Collections.Generic;
using Godot;
using ZoomBees.scripts.Weapons;

namespace ZoomBees.scripts; 

public partial class PlayerController : CharacterBody3D {
  // Read-only attributes
  private readonly List<Weapon.WeaponNames> _startWeapons = new() { Weapon.WeaponNames.Revolver, Weapon.WeaponNames.LaserGun };
  
  // Events
  public event Action<float> ChangedPlayerHealth;
  public event Action<Weapon.WeaponNames> ChangedWeapon;
  public event Action ShotFired; // => for stats
  public event Action PlayerKilled;
  
  // Camera attributes
  [Export] private float _tBobbling;
  [Export] private float _bobblingAmplitude = 0.08f;
  [Export] private float _bobblingFrequency = 2f;
  [Export] private float _fovBase = 75f;
  [Export] private float _fovChange = 1.5f;
  [Export] private float _sensitivity = 0.01f;
  
  // Movement attributes
  [Export] private float _jumpControl = 3f;
  [Export] private float _jumpVelocity = 4.5f;
  [Export] private float _speed = 5f;
  [Export] private float _sprintModifier = 1.75f;
  
  // Game mechanic attributes
  [Export] private const int MaxHealth = 5;
  
  // Properties
  private int Health {
    get => _health;
    set {
      _health = value;
      ChangedPlayerHealth?.Invoke((float)_health/MaxHealth);
      if (Health < MaxHealth) _healTimer.Start();
    }
  }
  
  // Game Nodes
  private LevelController _level;
  private Camera3D _camera;
  private Node3D _head;
  private Timer _healTimer;
  private Node3D _targetEnd;
  private RayCast3D _targetRayCast;
  
  // Mechanics
  private int _health;
  private Weapon _currentWeapon;  // The current weapon object
  private int _currentWeaponId;  // The index of the currently equipped weapon in "_equippedWeapons"
  private List<Weapon.WeaponNames> _equippedWeapons;  // All weapons the player currently owns
  private List<Weapon> _weapons;  // All available weapons // TODO: put this in a more global location
  
  // Game Cycle Methods
  public override void _Ready() {
    _level = GetParent<LevelController>();
    _camera = GetNode<Camera3D>("Head/Camera");
    _head = GetNode<Node3D>("Head");
    _healTimer = GetNode<Timer>("HealTimer");
    _targetEnd = GetNode<Node3D>("Head/Camera/RayCast/MaxRange");
    _targetRayCast = GetNode<RayCast3D>("Head/Camera/RayCast");

    Health = MaxHealth;
    
    _weapons = LoadWeapons();
    _currentWeaponId = 0;
    _equippedWeapons = _startWeapons;
    SetCurrentWeapon(_currentWeaponId);
    
    _healTimer.Timeout += OnTickedHealTimer;
  }

  public override void _PhysicsProcess(double delta) {
    // Apply sprint multiplier if needed
    float speed = Input.IsActionPressed("sprint") ? _speed * _sprintModifier : _speed;
    
    // Local copy of Velocity struct to modify its members
    Vector3 velocity = Velocity;
    
    // Apply gravity
    if (!IsOnFloor()) {
      velocity.Y -= _level.Gravity * (float)delta;
    }
    
    // Player inputs
    velocity = HandleJump(velocity);
    velocity = HandleMove(delta, velocity, speed);

    // Apply bobbling to the camera origin (need to modify the full transform since it is a C# struct)
    _camera.Transform = new Transform3D(_camera.Transform.Basis, HandleBobbling(delta, speed)); 
    
    // Apply FoV transformation
    float velocityClamped = Mathf.Clamp(velocity.Length(), 0.5f, _speed * _sprintModifier * 2);
    float targetFov = _fovBase + _fovChange * velocityClamped;
    _camera.Fov = Mathf.Lerp(_camera.Fov, targetFov, (float)delta * 8f);
    
    HandleWeaponSwitch();
    HandleShoot();
    
    // Apply velocity to the game node
    Velocity = velocity;
    MoveAndSlide();
  }

  public override void _UnhandledInput(InputEvent inputEvent) {
    if (inputEvent is InputEventMouseMotion @event) {
      _head.RotateY(-@event.Relative.X * _sensitivity);
      _camera.RotateX(-@event.Relative.Y * _sensitivity);

      Vector3 rotation = _camera.Rotation;
      rotation.X = Mathf.Clamp(rotation.X, Mathf.DegToRad(-70), Mathf.DegToRad(70));
      _camera.Rotation = rotation;
    }
  }
  
  public void ApplyDamage(int damage, Vector3 staggerDirection) {
    Health -= damage;
    if (Health == 0) PlayerKilled?.Invoke();
    
    Velocity += staggerDirection * 8f;
  }
  
  private void SetCurrentWeapon(int weaponId) {
    Weapon.WeaponNames name = _equippedWeapons[weaponId];
    
    GD.Print($"Asked for: {name}");
    
    foreach (Node child in _camera.GetChildren()) {
      if (child is Weapon weapon) {
        GD.Print($"Looking at {weapon.WeaponName}");
        if (weapon.WeaponName != name) {
          weapon.Visible = false;
        } else {
          GD.Print("Activated!");  // TODO: need to rethink the chronology, it needs some time spacing
          if (_currentWeapon is not null) {
            _currentWeapon.SheathThenDraw(weapon);  
            _currentWeapon = weapon;
            weapon.Visible = true;
          } else {
            _currentWeapon = weapon;
            weapon.Visible = true;
            _currentWeapon.Draw();
          }
          
          ChangedWeapon?.Invoke(name);
        }
      }
    }
  }
  
  // Other Methods
  private Vector3 HandleBobbling(double delta, float speed) {
    _tBobbling += IsOnFloor() ? (float)delta * Velocity.Length() : 0;
    
    Vector3 newHeadPosition = Vector3.Zero;
    newHeadPosition.Y = Mathf.Sin(_tBobbling * _bobblingFrequency) * _bobblingAmplitude;
    newHeadPosition.X = Mathf.Cos(_tBobbling * _bobblingFrequency / 2) * _bobblingAmplitude;
    
    return newHeadPosition;
  }
  
  private Vector3 HandleJump(Vector3 velocity) {
    if (Input.IsActionJustPressed("jump") && IsOnFloor()) {
      velocity.Y = _jumpVelocity;
    }

    return velocity;
  }

  private Vector3 HandleMove(double delta, Vector3 velocity, float speed) {
    Vector2 input = Input.GetVector("move_left", "move_right", "move_forward", "move_backward");
    Vector3 direction = _head.Transform.Basis * new Vector3(input.X, 0, input.Y).Normalized();

    if (IsOnFloor()) {
      if (direction != Vector3.Zero) {  // Player moves
        velocity.X = direction.X * speed;
        velocity.Z = direction.Z * speed;
      } else {  // Player stopped moving
        velocity.X = Mathf.Lerp(velocity.X, direction.X * speed, (float)delta * _jumpControl * 2f);
        velocity.Z = Mathf.Lerp(velocity.Z, direction.Z * speed, (float)delta * _jumpControl * 2f);
      }
    } else {  // Player is in the air
      velocity.X = Mathf.Lerp(velocity.X, direction.X * speed, (float)delta * _jumpControl);
      velocity.Z = Mathf.Lerp(velocity.Z, direction.Z * speed, (float)delta * _jumpControl);
    }
    

    return velocity;
  }

  private void HandleShoot() {
    if (!Input.IsActionJustPressed("shoot")) return;
    
    Vector3 aimingAt = (_targetRayCast.IsColliding()) ? _targetRayCast.GetCollisionPoint() : _targetEnd.GlobalPosition;
    if (_targetRayCast.GetCollider() is IDamageable target) {
      _currentWeapon.Attack(aimingAt, target);
    }
    else {
      _currentWeapon.Attack(aimingAt, null);
    }
  }
  
  private void HandleWeaponSwitch() {
    if (!Input.IsActionJustPressed("change_weapon")) return;
    if (!_currentWeapon.IsReady()) return;  // Cannot change weapon while charging/firing
    if (_equippedWeapons.Count < 2) return;  // If only one weapon, no need to change weapon

    _currentWeaponId = (_currentWeaponId < _equippedWeapons.Count-1) ? _currentWeaponId+1 : 0;
    SetCurrentWeapon(_currentWeaponId);
  }
  
  private void OnTickedHealTimer() {
    if (Health == MaxHealth) {
      GD.PushWarning("The timer should not have been launched!");
      return;
    }

    Health += 1;
  }

  private List<Weapon> LoadWeapons() {
    List<Weapon> weapons = new();

    foreach (Node child in _camera.GetChildren()) {
      if (child is Weapon weapon) {
        weapons.Add(weapon);
      }
    }

    return weapons;
  }

}