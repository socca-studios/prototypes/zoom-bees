using Godot;

namespace ZoomBees.scripts.Weapons; 

public partial class EnergyGun : Gun {
  public override void _Ready() {
    base._Ready();
  }
  
  public override void Attack(Vector3 aimingAt, IDamageable target) {
    if (!IsReady() || !CanAttack) return;
    
    AnimationPlayer.Play("Attack");
    
    EnergyBeam energyBeam = GameMgr.Assets.Objects["laser_beam"].Instantiate<EnergyBeam>();
    
    energyBeam.DrawMesh(Nozzle.GlobalPosition, aimingAt);  // Draw the laser beam
    FindParent("Level").GetNode<Node3D>("Ammo").AddChild(energyBeam);

    target?.Hit(aimingAt);  // Impact the target
    
    Particles.Emitting = true;
    
    if (Cooldown > 0) {
      CanAttack = false;
      GetTree().CreateTimer(Cooldown).Timeout += () => { CanAttack = true; };
    }
  }

}