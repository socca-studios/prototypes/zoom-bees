using Godot;

namespace ZoomBees.scripts.Weapons; 

public partial class ProjectileGun : Gun {
  public override void _Ready() {
    base._Ready();
  }
  
  public override void Attack(Vector3 aimingAt, IDamageable target) {
    if (!IsReady() || !CanAttack) return;
    
    AnimationPlayer.Play("Attack");
    
    Projectile bullet = GameMgr.Assets.Objects["bullet"].Instantiate<Projectile>();
    bullet.Position = Nozzle.GlobalPosition;
    FindParent("Level").GetNode<Node3D>("Ammo").AddChild(bullet);
    bullet.SetVelocityTo(aimingAt);
    
    Particles.Emitting = true;
    
    if (Cooldown > 0) {
      CanAttack = false;
      GetTree().CreateTimer(Cooldown).Timeout += () => {CanAttack = true; };
    }
  }
}