using Godot;

namespace ZoomBees.scripts.Weapons; 

public abstract partial class Gun : Weapon {
  [Export] protected float Dispersion;
  [Export] protected float Range;
  
  protected Node3D Nozzle;
  
  public override void _Ready() {
    base._Ready();
    Nozzle = GetNode<Node3D>("Frame/Nozzle");
  }
}