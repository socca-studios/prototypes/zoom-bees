using Godot;

namespace ZoomBees.scripts.Weapons; 

public partial class Projectile : Node3D {
  private float _speed = 30f;

  private MeshInstance3D _mesh;
  private GpuParticles3D _particles;
  private RayCast3D _rayCast;
  private Timer _timer;

  private Vector3 _velocity = Vector3.Zero;

  public override void _Ready() {
    _mesh = GetNode<MeshInstance3D>("Mesh");
    _particles = GetNode<GpuParticles3D>("Particles");
    _rayCast = GetNode<RayCast3D>("RayCast");
    _timer = GetNode<Timer>("LifeTimer");
    _timer.Timeout += QueueFree;
  }

  public override void _PhysicsProcess(double delta) {
    Position += _velocity * (float)delta;
    _rayCast.ForceRaycastUpdate();

    if (_rayCast.IsColliding()) {
      // GD.Print($"Collided with {_rayCast.GetCollider()}");
      _particles.Emitting = true;
      _mesh.Visible = false;
      _rayCast.Enabled = false;
      
      if (_rayCast.GetCollider() is IDamageable target) {
        target.Hit(GlobalPosition);
      }
      
      QueueFree();
    }
  }
  
  public void SetVelocityTo(Vector3 target) {
    LookAt(target);
    _velocity = Position.DirectionTo(target) * _speed;
  }
}