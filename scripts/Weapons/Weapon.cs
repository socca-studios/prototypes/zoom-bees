using Godot;
using ZoomBees.scripts.Managers;

namespace ZoomBees.scripts.Weapons; 

public abstract partial class Weapon : Node3D {
  public enum WeaponNames {
    Revolver, LaserGun
  }
  
  [Export] public WeaponNames WeaponName;
  [Export] protected float Cooldown;
  
  protected GameManager GameMgr;
  
  protected AnimationPlayer AnimationPlayer;
  protected GpuParticles3D Particles;

  protected bool CanAttack;

  public override void _Ready() {
    GameMgr = GetNode<GameManager>("/root/Game");
    
    AnimationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
    Particles = GetNode<GpuParticles3D>("Frame/Particles");
    
    CanAttack = true;
  }

  public abstract void Attack(Vector3 aimingAt, IDamageable target);

  public void Draw() {
    AnimationPlayer.Play("Draw");
  }

  public bool IsReady() {
    return !AnimationPlayer.IsPlaying();
  }
  
  // TODO: check if this works.
  public void SheathThenDraw(Weapon newWeapon) {
    AnimationPlayer.PlayBackwards("Draw");
    GetTree().CreateTimer(AnimationPlayer.GetAnimation("Draw").Length).Timeout += newWeapon.Draw;
  }
  
}