using Godot;

namespace ZoomBees.scripts.Weapons; 

public partial class EnergyBeam : MeshInstance3D {
  private Timer _timer;

  private float _alpha = 0.1f;
  private StandardMaterial3D _mat;
  
  public override void _Ready() {
    // Duplicate the material to ensure it is unique to the instance
    _mat = (StandardMaterial3D)MaterialOverride.Duplicate();
    MaterialOverride = _mat;
    
    _timer = GetNode<Timer>("Timer");
    _timer.Timeout += QueueFree;
  }

  public override void _PhysicsProcess(double delta) {
    FadeBeam(delta);
  }
  
  public void DrawMesh(Vector3 start, Vector3 end) {
    ImmediateMesh mesh = new ImmediateMesh();
    Mesh = mesh;
    mesh.SurfaceBegin(Mesh.PrimitiveType.Lines, MaterialOverride);
    mesh.SurfaceAddVertex(start);
    mesh.SurfaceAddVertex(end);
    mesh.SurfaceEnd();
  }

  private void FadeBeam(double delta) {
    _alpha -= (float)delta * 0.5f;
    Color color = _mat.AlbedoColor;
    color.A = _alpha;
    ((StandardMaterial3D)MaterialOverride).AlbedoColor = color;
  }
}