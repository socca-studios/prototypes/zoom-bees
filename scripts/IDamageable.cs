using Godot;

namespace ZoomBees.scripts; 

public interface IDamageable {
  public abstract void Hit(Vector3 impactPosition);  // Should be the code from BodyPart.Hit()
}