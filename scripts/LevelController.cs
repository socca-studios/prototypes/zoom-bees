using System;
using System.Collections.Generic;
using Godot;
using ZoomBees.scripts.Managers;
using ZoomBees.scripts.Weapons;

namespace ZoomBees.scripts; 

public partial class LevelController : Node {
  public float Gravity { get; private set; }
  public PlayerController Player { get; private set; }
  
  // Internal Game Nodes
  private GameManager _gameMgr;
  private Node3D _mobSpawnPoints;
  private NavigationRegion3D _navRegion;
  private Node3D _playerSpawnPoints;
  private Timer _spawnTimer;

  // UI Nodes
  private ColorRect _healthBar;
  private TextureRect _hitMarker;
  private Dictionary<Weapon.WeaponNames, TextureRect> _crossHairs;
  
  // Attributes
  private float _aimedHealthAlbedo = 0f;
  
  public override void _Ready() {
    // Initialising and Settings
    Gravity = (float)ProjectSettings.GetSetting("physics/3d/default_gravity");

    // Game Nodes
    _gameMgr = GetNode<GameManager>("/root/Game");
    _mobSpawnPoints = GetNode<Node3D>("Map/MobSpawns");
    _navRegion = GetNode<NavigationRegion3D>("Map/NavigationRegion");
    _playerSpawnPoints = GetNode<Node3D>("Map/PlayerSpawns");
    _spawnTimer = GetNode<Timer>("SpawnTimer");
    _spawnTimer.Timeout += OnZombieSpawnTimeout;
    
    // UI Nodes
    _crossHairs = new Dictionary<Weapon.WeaponNames, TextureRect> {
      { Weapon.WeaponNames.Revolver, GetNode<TextureRect>($"UI/CrossHair_Revolver") },
      { Weapon.WeaponNames.LaserGun, GetNode<TextureRect>($"UI/CrossHair_LaserGun") }
    };
    _healthBar = GetNode<ColorRect>("UI/HealthBar");
    _hitMarker = GetNode<TextureRect>("UI/HitMarker");

    SpawnPlayer();
  }

  public override void _Process(double delta) {
    Color color = _healthBar.Color;
    if (color.A > _aimedHealthAlbedo) {
      color.A = Mathf.Lerp(color.A, _aimedHealthAlbedo, (float)delta * 2.5f);
    } else if (color.A < _aimedHealthAlbedo) {
      color.A = _aimedHealthAlbedo;
    }
    
    _healthBar.Color = color;
  }

  private void OnPlayerHit(Vector3 staggerDirection) {
    Player.ApplyDamage(1, staggerDirection);
  }

  private void OnZombieSpawnTimeout() {
    Vector3 spawnPosition = GetRandomSpawnPoint(_mobSpawnPoints);
    GhoulController newGhoul = _gameMgr.Assets.Actors["ghoul"].Instantiate<GhoulController>();
    newGhoul.HitPlayer += OnPlayerHit;
    newGhoul.Impacted += ImpactedSomething;
    newGhoul.ImpactedCriticalPart += () => { _gameMgr.Headshots += 1; };
    newGhoul.KilledGhoul += () => { _gameMgr.Kills += 1; };
    newGhoul.Position = spawnPosition;
    _navRegion.AddChild(newGhoul);
  }

  private void SpawnPlayer() {
    Vector3 spawnPosition = GetRandomSpawnPoint(_playerSpawnPoints);
    PlayerController player = _gameMgr.Assets.Actors["player"].Instantiate<PlayerController>();
    Player = player;
    Player.ChangedPlayerHealth += OnPlayerHealthChanged;
    Player.PlayerKilled += OnPlayerKilled;
    Player.ChangedWeapon += OnWeaponChanged;
    Player.ShotFired += () => { _gameMgr.Shots += 1; };
    player.Name = "Player";
    player.Position = spawnPosition;
    AddChild(player);
  }
  
  private Vector3 GetRandomSpawnPoint(Node3D parentNode) {
    int randomId = new Random().Next(parentNode.GetChildren().Count);
    Node3D spawn = parentNode.GetChild<Node3D>(randomId);
    return spawn.GlobalPosition;
  }
  
  private void ImpactedSomething() {
    _gameMgr.Touches += 1;
    _hitMarker.Visible = true;
    GetTree().CreateTimer(0.05f).Timeout += () => { _hitMarker.Visible = false; };
  }

  private void OnPlayerHealthChanged(float healthPercentage) {
    _aimedHealthAlbedo = (1 - healthPercentage);
    GD.Print($"Life: {(healthPercentage)}, Aimed: {_aimedHealthAlbedo}, Current: {_healthBar.Color.A}");
  }
  
  private void OnPlayerKilled() {
    _gameMgr.ToEndMenu();
  }
  
  private void OnWeaponChanged(Weapon.WeaponNames newWeaponName) {
    foreach (Weapon.WeaponNames name in _crossHairs.Keys) {
      _crossHairs[newWeaponName].Visible = (newWeaponName == name) ? true : false;
    }
  }
  
}